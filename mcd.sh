#!/bin/sh
#

if [[ $# -ne 4 ]]; then
	echo "Usage: mcd-cmd.sh HOST PORT \"CMD\" FILE"
	exit 1
fi

HOST=$1
PORT=$2
CMD=$3
JSON=$4
FILE=$(mktemp)

echo $CMD > $FILE
cat $JSON >> $FILE

ncat $HOST $PORT < $FILE

rm -f $FILE
