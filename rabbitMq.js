var amqp = require('amqp');

var options = {
    host: 'localhost',
    port: 5672,
    login: 'guest',
    password: 'guest',
    authMechanism: 'AMQPLAIN',
    vhost: '/'
}
var connection = amqp.createConnection(options, {
    defaultExchangeName: "TAP.Commands"
});

// add this for better debuging
var _exchange = null;
var _queue = null;
var _consumerTag = null;

// Report errors
connection.on('error', function(err) {
    console.error('Connection error', err);
});

// Update our stored tag when it changes
connection.on('tag.change', function(event) {
    if (_consumerTag === event.oldConsumerTag) {
        _consumerTag = event.consumerTag;
        // Consider unsubscribing from the old tag just in case it lingers
        _queue.unsubscribe(event.oldConsumerTag);
    }
});

// Initialize the exchange, queue and subscription
connection.on('ready', function() {
    console.log("Connection Ready")

    _exchange = connection.exchange("TAP.Commands", {
        passive: true
    })
    connection.queue('commandQueue', function(queue) {
        _queue = queue;
        console.log("queue ready")
        // Bind to the exchange
        queue.bind('TAP.Commands', 'commandBindingKey', () => {
            console.log("Command bindign")
            _exchange.publish("commandBindingKey", "api mcd campaign list")

        });
        queue.subscribe(function(message, headers, deliveryInfo, messageObject) {
            console.log('Got a message with routing key ' + deliveryInfo.routingKey);
            console.log(message.data.toString('utf8'))
            messageObject.acknowledge(true)
        }).addCallback(function(res) {
            // Hold on to the consumer tag so we can unsubscribe later
            _consumerTag = res.consumerTag;
        });


    });

    connection.queue('EventsQueue', function(queue) {
        _queue = queue;
        console.log("queue ready")
        // Bind to the exchange
        queue.bind('TAP.Events', () => {
            console.log("EVENTS bindign")
        });
        queue
            .subscribe(function(message) {
                // Handle message here
                console.log('Got message events', message);
                queue.shift(false, false);
            })
            .addCallback(function(res) {
                // Hold on to the consumer tag so we can unsubscribe later
                _consumerTag = res.consumerTag;
            });
    });

    // Some time in the future, you'll want to unsubscribe or shutdown
    setTimeout(function() {
        if (_queue) {
            _queue
                .unsubscribe(_consumerTag)
                .addCallback(function() {
                    // unsubscribed
                });
        } else {
            // unsubscribed
        }
    }, 60000);


})
