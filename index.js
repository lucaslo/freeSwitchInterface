var telnet = require("./telnet")

var nbash = require('./bashNcat')

processInstruction = nbash.processInstruction

var fs = require('fs');
var path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
var readline = require('readline');
var rl = readline.createInterface(process.stdin, process.stdout);


function getId(words) {
    var n = words.split(" ");
    return n[n.length - 1].replace(/(\r\n\t|\n|\r\t)/gm,"");
}

var getNthWord = function(string, n) {
    var words = string.split(" ");
    return words[n - 1].replace(/(\r\n\t|\n|\r\t)/gm,"");
}




var connectionHandler = telnet.connection

function checkIfIsFile(filePath){
  const newPath = path.resolve(filePath)
  if (!fs.lstatSync(newPath).isFile()) {
      console.error("File path is not actually an existing file")
      return false
  }

  return true
}

async function command() {

  rl.setPrompt('freeSwitch> ');
  rl.prompt();
  rl.on('line', async function(line) {
      var response
      if (line === "exit") rl.close();
      var command = getNthWord(line, 1)
      switch (command) {
          case "create":
              var createPath = getNthWord(line,2)
              if(!checkIfIsFile(createPath)) break
              response = await processInstruction("./mcd.sh localhost 54322 'mcd campaign create' " +createPath)
              campaignId = getId(response)
              console.log("Campaign created. New campaignId:"+ campaignId)
              break;
            case "lead":
                if(campaignId===undefined){
                  campaignId = getNthWord(line,3)
                  if(campaignId===undefined){
                    console.log("No campaign Id is set")
                    break
                  }
                }
                var leadPath = getNthWord(line,2)
                if(!checkIfIsFile(leadPath)) break
                response = await processInstruction("./mcd.sh localhost 54322 'mcd campaign leads add "+ campaignId + "' " +leadPath)
                console.log(response)
                break;
            case "assign-agent":
                if(campaignId===undefined){
                  campaignId = getNthWord(line,3)
                  if(campaignId===undefined){
                    console.log("No campaign Id is set")
                    break
                  }
                }
                var user = getNthWord(line,2)
                console.log("api mcd campaign agent register "+ campaignId + " " +user + "\n\n")
                response = await telnetConnection.send("api mcd campaign agent register "+ campaignId + " " +user + "\n\n", function(err, res){
                  })
                console.log(response)
                break;
              case "start":
                  if(campaignId===undefined){
                    campaignId = getNthWord(line,2)
                    if(campaignId===undefined){
                      console.log("No campaign Id is set")
                      break
                    }
                  }
                  response = await telnetConnection.send("api mcd campaign start " + campaignId +"\n\n", function(err, res){
                      console.log(res)
                    })
                  break;
              case "help":
                if(campaignId === undefined){
                  console.log("campaigId is not set. You can set one and save it for all commands or inform in each individual command")
                } else {
                  console.log("Campaign id: " + campaignId)
                }
                console.log("create <json>")
                console.log("lead <json> <campaignId>")
                console.log("assign-agent <user> <campaignId> ")
                console.log("start <campaigId>")
                break;
		break;
          case "setCampaignId":
              campaignId = getNthWord(line, 2)
              console.log("New Campaign id: " + campaignId)

              break;
          default:
              response = await telnetConnection.send(line+"\n\n", function(res, err){
                  console.log("Telnet Response:")
                  console.log(err)
                })
              break;
      }
      rl.prompt();
  }).on('close', function() {
      process.exit(0);
  });
}

command()
