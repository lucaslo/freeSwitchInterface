#!/bin/sh
#

if [[ $# -ne 3 ]]; then
	echo "Usage: mcd-cmd.sh HOST PORT \"CMD\" FILE"
	exit 1
fi

HOST=$1
PORT=$2
CMD=$3

echo $CMD > $FILE

ncat $HOST $PORT < $FILE

rm -f $FILE
