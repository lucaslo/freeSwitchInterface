
var fs = require('fs');
var path = require ('path');
var Telnet = require('telnet-client')
var connection = new Telnet()

var commands = []

connection.on('ready', async function(prompt) {
});

connection.on('timeout', function() {
  console.log('socket timeout!')
  connection.end();
  process.exit(0);
});


connection.on('failedLogin', function() {
  console.log("failedLogin")
});

connection.on('close', function() {
  console.log('connection closed');
  process.exit(0);
});

connection.on('error', function(error) {
  console.log('connection error');
  console.log(error)
});


function connectionHandler(){
  var address = process.argv[2]
  var port = process.argv[3]

  if(address===undefined){
    console.log("No address defined. Run 'node index.js <address> <port>'")
    process.exit(0);
  }
  if(port ===undefined){
    console.log("No port defined. Run 'node index.js <address> <port>' ")
    process.exit(0);
  }

  var params = {
    host: address,
    port: port,
    timeout: 6000000,
    passwordPrompt: "Content-Type: auth/request",
    password:"auth ClueCon\n",
    shellPrompt:"",
    negotiationMandatory: false,
    debug: true
    // removeEcho: 4
  };
  connection.connect(params).catch((err) =>{
    console.log(err)
    console.log("Something went wront with the telnet connection.")
    process.exit(0);
  });

  return connection

}
exports.connection = connectionHandler()

var fs = require('fs');
var path = require ('path');



var Telnet = require('telnet-client')
var connection = new Telnet()
var params = {
  host: '127.0.0.1',
  port: 8021,
  timeout: 6000000,
  passwordPrompt: "Content-Type: auth/request",
  password:"auth ClueCon\n",
  shellPrompt:"",
  negotiationMandatory: false,
  debug: true
  // removeEcho: 4
};
var commands = []

connection.on('ready', async function(prompt) {
});

connection.on('timeout', function() {
  console.log('socket timeout!')
  connection.end();
  process.exit(0);
});


connection.on('failedLogin', function() {
  console.log("failedLogin")
});

connection.on('close', function() {
  console.log('connection closed');
});

connection.on('error', function(error) {
  console.log('connection error');
  console.log(error)
});


connection.connect(params).catch((err) =>{
  console.log(err)
});

async function menu(){
  let stream = ""
  let result = ""
  let command = ""
  rl.setPrompt('freeSwitch> ');
  rl.prompt();
  rl.on('line', async function(line) {
      if (line === "right") rl.close();
      result = await connection.send(line+"\n\n", function(response, err){
        console.log("Response:")
        console.log(response)
        console.log("err")
        console.log(err)
      })
      rl.prompt();
  }).on('close',function(){
      process.exit(0);
  });
}

exports.connection = connection
