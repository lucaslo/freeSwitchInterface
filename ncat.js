var Netcat = require('node-netcat')
var fs = require('fs');
var path = require ('path');
var scan = require('node-netcat').portscan();
scan.run('127.0.0.1', '54322', function(err, res) {
  console.log("SCAN")
	if (err) {
		console.log(err)
	} else {
    console.log(res)
	}
});


const filePath = path.resolve(process.argv[2])

if (!fs.lstatSync(filePath).isFile()) {
  console.error("File path is not actually an existing file")
  process.exit(1)
}

// Load json with events
var data = require(filePath)
var options = {
 // define a connection timeout
	timeout: 60000,
 // buffer(default, to receive the original Buffer objects), ascii, hex,utf8, base64
  read_encoding: 'ascii'
 }


var client = Netcat.client(54322, '127.0.0.1',options);

client.on('open', function () {
  console.log('connect');
  client.exec("mcd campaign create" +data);
});

client.on('data', function (response) {
  console.log("received data")
  console.log(response);
});

client.on('error', function (err) {
  console.log(err);
});

client.on('close', function () {
  console.log('close');
});

client.start();
