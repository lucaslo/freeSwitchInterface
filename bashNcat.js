const util = require('util');
const exec = util.promisify(require('child_process').exec);

async function processInstruction(instruction){
  const {stdout, stderr} = await exec(instruction).catch(err => {
      console.log(err)
      process.exit(1)
  })

  return stdout
}

exports.processInstruction = processInstruction
