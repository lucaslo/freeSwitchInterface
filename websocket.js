var io = require("socket.io-client")

let socket = null

var address = process.argv[2]
var port = process.argv[3]

if(address===undefined){
  console.log("No address defined. Run 'node websocket.js <address> <port>'")
  process.exit(0);
}
if(port ===undefined){
  console.log("No port defined. Run 'node websocket.js <address> <port>' ")
  process.exit(0);
}


export function initSocket (nick) {

  socket = io(address+":"+port, {
    forceNew: true,
    transports: ["websocket"],
    upgrade: false,
  })
  socket.on("connect", ()=>{
    console.log("connection open")

  })
  socket.on("connect_error", ()=>{
    console.log("connection timed out")
    process.exit(0);

  })
  socket.on("error", (message)=>{
    console.log("connection error")
    process.exit(0);


  })
  socket.on("message", (message)=>{
    console.log(message)
    console.log("message")
  })
  socket.on("disconnect", (message)=>{
    console.log("disconnected")
    process.exit(0);
  })

  socket.on("close", (message)=>{
    console.log("closed")
    process.exit(0);
  })
}


initSocket()
